var url = "http://SERVER_ADDRESS:3005";
var token = "ACCESS_TOKEN";
var hub = "mainHub";

var mPosNotify = require("../src/index.js"); // require("mpos-notify-browser"); in production

var Notifier = mPosNotify.Notifier;

var n = new Notifier(url, hub);

n.connect(
    token,
    function (id) {
        console.log("connected");
        console.log(id);

        var s = n.subscribe("ArticleCategoryUpdated", function (data) {
            console.log("event");
            console.log(data);
        });
        console.log(s);

        s = n.unsubscribe("ArticleCategoryUpdated");
        console.log(s);

        s = n.disconnect();
        console.log(s);
    },
    function (error) {
        console.log("error");
        console.log(error);
    }
);
