var _ = require("underscore");
var $ = require("jquery");
window.jQuery = $;
require("ms-signalr-client");

var Exception = require("./Exception");

function Notifier (url, hub) {
    this.url = url;
    this.hub = hub;

    this.connection = {};
    this.proxyHub = {};

    this.subscribers = {};
}
Notifier.prototype.connect = function (token, onSuccess, onError) {
    this.connection = $.hubConnection(this.url, { qs: { Bearer: token } });
    this.connection.error(onError);

    this.proxyHub = this.connection.createHubProxy(this.hub);

    var self = this;
    this.proxyHub.on("OnEvent", function (notification) {
        if (!_.isUndefined(self.subscribers[notification.EventName])) {
            self.subscribers[notification.EventName](notification);
        }
    });

    this.connection.start()
        .done(function() {
            onSuccess(self.connection.id);
        })
        .fail(function() {
            onError(new Exception("Connection failed", 0)); // TODO: Messages
        });
};
Notifier.prototype.disconnect = function () {
    if (!_.isUndefined(this.connection)) {
        this.connection.stop();
        return true;
    }
    return false;
};
Notifier.prototype.subscribe = function (eventName, callback, force) {
    if (!_.isUndefined(this.proxyHub)) {
        if (_.isUndefined(this.subscribers[eventName]) || force === true) {
            this.proxyHub.invoke("Subscribe", { "EventName": eventName });
            this.subscribers[eventName] = callback;
            return true;
        }
    }
    return false;
};
Notifier.prototype.unsubscribe = function (eventName) {
    if (!_.isUndefined(this.proxyHub)) {
        this.proxyHub.invoke("Unsubscribe", { "EventName": eventName }); // TODO: Not working - server return false
        if (_.has(this.subscribers, eventName)) {
            delete this.subscribers[eventName];
        }
        return true;
    }
    return false;
};

module.exports = Notifier;
