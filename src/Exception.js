function Exception (message, code) {
    this.Message = message;
    this.Code = code;
}

module.exports = Exception;
